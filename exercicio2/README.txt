Linha 11:
Membros declarados com visibilidade implicita (package) sem real necessidade,
poderiam ser completamente substituídos por variáveis locais aos métodos. 

Linha 23:
Uso de raw type na assinatura do método (tipo recomendado seria List<Integer>),
abre a possibilidade de passagem de uma coleção de tipo não suportado (por exemplo,
um ArrayList de String ou qualquer outra classe), o que provocaria erros em tempo de
execução.

Linha 32:
Concatenação direta de strings, caso este método seja invocado
muitas vezes dentro de um loop  pode gerar impacto em termos de uso da 
PermGen na JVM e na performance como um todo. A prática recomendada é
o uso de StringBuilder para construção de strings dinâmicas.

Linha 42:
Uso de raw type na assinatura do método (tipo recomendado seria List<Integer>),
abre a possibilidade de passagem de uma coleção de tipo não suportado (por exemplo,
um ArrayList de String ou qualquer outra classe), o que provocaria erros em tempo de
execução.

Linha 46:
Falta de consistência na implementação; deveria ou utilizar 2 constantes
imediatas ou declarar 2 membros estáticos. Da forma que está torna a manutenção
e a compreensão do código mais trabalhosa.

Linha 58:
Como lista não tem um tipo pré-definido pela assinatura do método,
torna necessária conversão não-checada para obter o Iterator, o que abre a
possiblidade exceções se for passado como parâmetro um tipo não suportado 
(por exemplo, um ArrayList de Double ou BigDecimal).

Linha 67:
Uso de if...else sem delimitadores de bloco torna a leitura
código mais difícil

Ainda na linha 67:
Conversão desnecessária de cod para string para determinar se cod está vazio,
poderia verificar diretamente cod.length().

Ainda na linha 67:
O método length() nunca retorna valores negativos, portanto
o teste "cod.toString().length() <= 0" deveria ser "cod.toString().length() == 0"

Linha 75:
Concatenação direta de strings, a prática recomendada 
seria o uso de StringBuilder. Vide explicação anterior.

Linha 80:
Concatenação direta de strings, a prática recomendada 
seria o uso de StringBuilder. Vide explicação anterior.		