package softplan.exercicio2;

import java.util.List;
/**
 * Classe alternativa a GeradorObservacao
 * @author chpinto
 */
public class GeradorObsAlternativo {

	// Gera observações, com texto pre-definido, incluindo os números, das notas
	// fiscais, recebidos no parâmetro
	public String geraObservacao(List<Integer> lista) {
		StringBuilder sb = new StringBuilder();
		if ((lista != null) && (!lista.isEmpty())) {
			sb.append("Fatura ")
				.append(lista.size() == 1 ? "da nota fiscal " : "das notas fiscais ")
				.append("de simples remessa: ");
			
			StringBuilder sb2 = new StringBuilder();
			int c = 0;
			for (Integer nf : lista) {
				if ((sb2.length() > 0) && (c == lista.size() - 1)) {
					sb2.append(" e ");
				} else if (sb2.length() > 0) {
					sb2.append(", ");
				}
				
				sb2.append(nf.toString());
				c++;
			}
			
			sb.append(sb2.toString()).append(".");
		}

		return sb.toString();
	}
}
