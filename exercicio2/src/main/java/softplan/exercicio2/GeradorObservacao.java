package softplan.exercicio2;


import java.util.Iterator;
import java.util.List;

public class GeradorObservacao { 
	// Carlos : membros declarados com visibilidade implicita (package) sem real necessidade,
	// poderiam ser completamente substituídos por variáveis locais aos métodos. 
	//Textos pr�-definidos
	static final String umoNota = "Fatura da nota fiscal de simples remessa: ";
	//Identificador da entidade
	String texto;
		
	//Gera observa��es, com texto pre-definido, incluindo os n�meros, das notas fiscais, recebidos no par�metro
	/* (non-Javadoc)
	 * @see softplan.exercicio2.Gerador#geraObservacao(java.util.List)
	 */
	// Carlos: Uso de raw type na assinatura do método (tipo recomendado seria LIst<Integer>),
	// abre a possibilidade de passagem de uma coleção de tipo não suportado (por exemplo,
	// um ArrayList de String ou qualquer outra classe), o que provocaria erros em tempo de
	// execução.
	public String geraObservacao(List lista)
	{
		texto = "";
		if (!lista.isEmpty()) 
		{
			// Carlos: concatenação direta de strings, caso este método seja invocado
			// muitas vezes dentro de um loop  pode gerar impacto em termos de uso da 
			// PermGen na JVM e na performance como um todo. A prática recomendada é
			// o uso de StringBuilder para construção de strings dinâmicas.
			return retornaCodigos(lista) + ".";
		}		
		return "";		
	}

	//Cria observa��o
	// Carlos: Uso de raw type na assinatura do método (tipo recomendado seria List<Integer>),
	// abre a possibilidade de passagem de uma coleção de tipo não suportado (por exemplo,
	// um ArrayList de String ou qualquer outra classe), o que provocaria erros em tempo de
	// execução.
	private String retornaCodigos(List lista) {
		// Carlos: falta de consistência na implementação; deveria ou utilizar 2 constantes
		// imediatas ou declarar 2 membros estáticos. Da forma que está torna a manutenção
		// e a compreensão do código mais trabalhosa.
		if (lista.size() >= 2) {
			texto = "Fatura das notas fiscais de simples remessa: ";
		} else {
			texto = umoNota;
		}
		
		//Acha separador
		StringBuilder cod = new StringBuilder();
		// Carlos: como lista não tem um tipo pré-definido pela assinatura do método,
		// torna necessária conversão não-checada para obter o Iterator, o que abre a
		// possiblidade exceções se for passado como parâmetro um tipo não suportado 
		// (por exemplo, um ArrayList de Double ou BigDecimal).
		for (Iterator<Integer> iterator = lista.iterator(); iterator.hasNext();) {
			Integer c = iterator.next();
			String s = "";
			// Carlos: uso de if...else sem delimitadores de bloco torna a leitura
			// do código mais difícil
			// Carlos: conversão desnecessária de cod para string para determinar
			// se está vazio (poderia verificar diretamente cod.length()
			// Carlos: o método length() nunca retorna valores negativos, portanto
			// o teste "cod.toString().length() <= 0" deveria ser "cod.toString().length() == 0"
			if( cod.toString() == null || cod.toString().length() <= 0 )
				s =  "";			
				else if( iterator.hasNext() )
					s =  ", ";
				else
					s =  " e ";
			// Carlos: concatenação direta de strings, a prática recomendada 
			// seria o uso de StringBuilder. Vide explicação anterior.
			cod.append(s + c);
		}

		// Carlos: concatenação direta de strings, a prática recomendada 
		// seria o uso de StringBuilder. Vide explicação anterior.		
		return texto + cod;
	}
}