package softplan.exercicio1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Mock de DAO para o exercício 1
 * @author Carlos Silva
 *
 */
public class MockExercicioDAO {
	
	// hashmap que simula tabelas
	private Map<String, Map<Long,Object>> tabelas = new HashMap<>();
	
	public MockExercicioDAO () {
		Map<Long,Object> tblTipoVia = new HashMap<>();
		Map<Long,Object> tblTipoVeiculo = new HashMap<>();
		Map<Long,Object> tblParametro = new HashMap<>();
		
		//tipos de via
		TipoVia tipoVia = new TipoVia();
		tipoVia.setId(1);
		tipoVia.setDescricao("Pavimentada");
		tipoVia.setCustoKmRodado((float)0.54);
		tblTipoVia.put(tipoVia.getId(), tipoVia);
		
		tipoVia = new TipoVia();
		tipoVia.setId(2);
		tipoVia.setDescricao("Não-Pavimentada");
		tipoVia.setCustoKmRodado((float)0.56);
		tblTipoVia.put(tipoVia.getId(), tipoVia);
		
		// tipos de veiculo
		TipoVeiculo tipoVeiculo = new TipoVeiculo();
		tipoVeiculo.setId(1);
		tipoVeiculo.setDescricao("Caminhao Bau");
		tipoVeiculo.setFator((float)1.00);
		tblTipoVeiculo.put(tipoVeiculo.getId(), tipoVeiculo);
		
		tipoVeiculo = new TipoVeiculo();
		tipoVeiculo.setId(2);
		tipoVeiculo.setDescricao("Caminhao Cacamba");
		tipoVeiculo.setFator((float)1.05);
		tblTipoVeiculo.put(tipoVeiculo.getId(), tipoVeiculo);
		
		tipoVeiculo = new TipoVeiculo();
		tipoVeiculo.setId(3);
		tipoVeiculo.setDescricao("Carreta");
		tipoVeiculo.setFator((float)1.12);
		tblTipoVeiculo.put(tipoVeiculo.getId(), tipoVeiculo);
		
		// parametros
		Parametro parametro = new Parametro();
		parametro.setId(Parametro.Chave.CARGA_MINIMA_TONELADAS.ordinal());
		parametro.setValor((float)5.0);
		tblParametro.put(parametro.getId(), parametro);
		parametro = new Parametro();
		parametro.setId(Parametro.Chave.CUSTO_TONELADA_EXTRA.ordinal());
		parametro.setValor((float)0.02);
		tblParametro.put(parametro.getId(), parametro);
		parametro = new Parametro();
		parametro.setId(Parametro.Chave.CUSTO_VIA_PAVIMENTADA.ordinal());
		parametro.setValor((float)0.54);
		tblParametro.put(parametro.getId(), parametro);
		parametro = new Parametro();
		parametro.setId(Parametro.Chave.CUSTO_VIA_NAO_PAVIMENTADA.ordinal());
		parametro.setValor((float)0.62);
		tblParametro.put(parametro.getId(), parametro);		
		
		// monta o hashmap final
		tabelas.put(TipoVia.class.getSimpleName(), tblTipoVia);
		tabelas.put(TipoVeiculo.class.getSimpleName(), tblTipoVeiculo);
		tabelas.put(Parametro.class.getSimpleName(), tblParametro);
	}
	
	/**
	 * Recupera todos os registros da pseudotabela inferida por entClass.
	 * @param entClass
	 * @return {@link List} de {@link AbstractEntity}, se for uma classe gerenciada por esse DAO,
	 * ou null se a classe não corresponder a uma das pseudotabelas.
	 */
	public List<AbstractEntity> findAll (Class<? extends AbstractEntity> entClass) {
		List<AbstractEntity> result = null;
		
		if (tabelas.containsKey(entClass.getSimpleName())) {
			Map<Long,Object> tbl = tabelas.get(entClass.getSimpleName());
			result = new ArrayList<>();
			for (Object obj : tbl.values()) {
				result.add(AbstractEntity.class.cast(obj));
			}
		}		
		
		return result;
	}
	
	/**
	 * Recupera um objeto da pseudotabela inferida a partir de entClass e pelo id.
	 * @param entClass
	 * @param id
	 * @return {@link AbstractEntity}, se for uma classe gerenciada por esse DAO,
	 * ou null se a classe não corresponder a uma das pseudotabelas.
	 */
	
	public AbstractEntity findById (Class<? extends AbstractEntity> entClass, long id) {
		AbstractEntity result = null;
		
		if (tabelas.containsKey(entClass.getSimpleName())) {
			Map<Long,Object> tbl = tabelas.get(entClass.getSimpleName());
			if (tbl.containsKey(id)) {
				result = (AbstractEntity) tbl.get(id);
			}
		}
		
		return result;
	}

}
