package softplan.exercicio1;

/**
 * Interface para as entidades persistidas.
 * @author Carlos Silva
 *
 */
public interface AbstractEntity {
	public long getId ();
	
}
