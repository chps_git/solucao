package softplan.exercicio1;

import java.math.BigDecimal;

import softplan.exercicio1.rest.CalculoParametro;

/**
 * Classe que realiza o cálculo propriamente dito do custo.
 * 
 * OBS: Esta classe seria idealmente um EJB stateless, porém devido a um conflito entre RestEASY e Weld
 * (que faz parte do WildFly), não consegui fazer a injeção usando @Inject funcionar de forma confiável,
 * por isso optei por implementar como um POJO simples. Uma alternativa seria o uso da annotation @EJB,
 * porém esta não é padronizada.
 * 
 * Referências (aparecem como "Resolved" ou "Closed", porém os erros persistem mesmo com as ultimas
 * versões estáveis do RestEASY e WildFly):
 * 
 * https://issues.jboss.org/browse/RESTEASY-1538?_sscc=t
 * https://issues.jboss.org/browse/RESTEASY-353
 * 
 * @author Carlos Silva
 *
 */
public class Calculo {
	public float calcularCustoTransporte(float dist1, float dist2, float custoVia1, float custoVia2,
			float fatorVeiculo, float peso, float pesoMinimo, float custoPesoExcedente) {
		float result = 0;

		if (dist1 > 0) {
			result = dist1 * custoVia1;
		}

		if (dist2 > 0) {
			result = result + (dist2 * custoVia2);
		}

		result = result * fatorVeiculo;
		if (peso > pesoMinimo) {
			float excedente = peso - pesoMinimo;
			if (dist1 > 0) {
				result = result + (excedente * custoPesoExcedente * dist1);
			}

			if (dist2 > 0) {
				result = result + (excedente * custoPesoExcedente * dist2);
			}
		}

		return new BigDecimal(result).setScale(2, BigDecimal.ROUND_HALF_UP).floatValue();
	}

	public float calcularCustoTransporte(CalculoParametro param) {
		return calcularCustoTransporte(param.getDist1(), param.getDist2(), param.getCustoVia1(), param.getCustoVia2(),
				param.getTipoVeiculo(), param.getToneladas(), param.getPesoMinimo(), param.getCustoPesoExcedente());
	}
}
