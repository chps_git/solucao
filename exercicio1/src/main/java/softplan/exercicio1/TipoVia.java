package softplan.exercicio1;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class TipoVia implements AbstractEntity {
	
	@Id
	private long id;
	
	private String descricao;
	private float custoKmRodado;

	@Override
	public long getId() {

		return id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setId(long id) {
		this.id = id;
	}

	public float getCustoKmRodado() {
		return custoKmRodado;
	}

	public void setCustoKmRodado(float custoKmRodado) {
		this.custoKmRodado = custoKmRodado;
	}

}
