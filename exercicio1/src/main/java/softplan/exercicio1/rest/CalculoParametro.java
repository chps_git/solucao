package softplan.exercicio1.rest;

public class CalculoParametro {
	private float dist1;
	private float dist2;
	private float tipoVeiculo;
	private float toneladas;
	private float custoVia1;
	private float custoVia2;
	private float pesoMinimo;
	private float custoPesoExcedente;
	
	public float getDist1() {
		return dist1;
	}
	
	public void setDist1(float dist1) {
		this.dist1 = dist1;
	}
	
	public float getDist2() {
		return dist2;
	}
	
	public void setDist2(float dist2) {
		this.dist2 = dist2;
	}
	
	public float getTipoVeiculo() {
		return tipoVeiculo;
	}
	
	public void setTipoVeiculo(float tipoVeiculo) {
		this.tipoVeiculo = tipoVeiculo;
	}
	
	public float getToneladas() {
		return toneladas;
	}
	
	public void setToneladas(float toneladas) {
		this.toneladas = toneladas;
	}

	public float getCustoVia1() {
		return custoVia1;
	}

	public void setCustoVia1(float custoVia1) {
		this.custoVia1 = custoVia1;
	}

	public float getCustoVia2() {
		return custoVia2;
	}

	public void setCustoVia2(float custoVia2) {
		this.custoVia2 = custoVia2;
	}

	public float getPesoMinimo() {
		return pesoMinimo;
	}

	public void setPesoMinimo(float pesoMinimo) {
		this.pesoMinimo = pesoMinimo;
	}

	public float getCustoPesoExcedente() {
		return custoPesoExcedente;
	}

	public void setCustoPesoExcedente(float custoPesoExcedente) {
		this.custoPesoExcedente = custoPesoExcedente;
	}
}
