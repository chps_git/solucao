package softplan.exercicio1.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;

import javax.ws.rs.core.Response;

import softplan.exercicio1.AbstractEntity;
import softplan.exercicio1.Calculo;
import softplan.exercicio1.ExercicioDAO;
import softplan.exercicio1.Parametro;
import softplan.exercicio1.TipoVeiculo;

@Path("calculo")
@Produces(MediaType.APPLICATION_JSON)
public class CalcResource {
	
	private ExercicioDAO dao = new ExercicioDAO();
	private Calculo calculo = new Calculo ();
	
	@Path("")
	@POST
	public Response updateCalculo(CalculoParametro parametros) {
		Map<String,String> result = new HashMap<>();		
		Parametro p = (Parametro)dao.findById(Parametro.class, Parametro.Chave.CARGA_MINIMA_TONELADAS.ordinal());
		parametros.setPesoMinimo(p.getValor());
		p = (Parametro)dao.findById(Parametro.class, Parametro.Chave.CUSTO_TONELADA_EXTRA.ordinal());
		parametros.setCustoPesoExcedente(p.getValor());	
		p = (Parametro)dao.findById(Parametro.class, Parametro.Chave.CUSTO_VIA_PAVIMENTADA.ordinal());
		parametros.setCustoVia1(p.getValor());
		p = (Parametro)dao.findById(Parametro.class, Parametro.Chave.CUSTO_VIA_NAO_PAVIMENTADA.ordinal());
		parametros.setCustoVia2(p.getValor());
		
		float custo = calculo.calcularCustoTransporte(parametros);
		result.put("custoTransporte", Float.toString(custo));
		GenericEntity<Map<String,String>> ge = new GenericEntity<Map<String,String>>(result){};
		return Response.ok(ge).build();
	}
	
	
	@Path("tipoVeiculo")
	@GET
	public Response findAllTipoVeiculo() {
		ExercicioDAO dao = new ExercicioDAO();
		List<AbstractEntity> tipos = dao.findAll(TipoVeiculo.class);
		GenericEntity<List<AbstractEntity>> ge = new GenericEntity<List<AbstractEntity>>(tipos){};
		return Response.ok(ge).build();		
	}
}
