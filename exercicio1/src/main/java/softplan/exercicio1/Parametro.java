package softplan.exercicio1;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Parametro implements AbstractEntity {

	public enum Chave {
		CARGA_MINIMA_TONELADAS,
		CUSTO_TONELADA_EXTRA,
		CUSTO_VIA_PAVIMENTADA,
		CUSTO_VIA_NAO_PAVIMENTADA
	}
	
	@Id
	private long id;
	private float valor;
	
	@Override
	public long getId() {
		return id;
	}

	public float getValor() {
		return valor;
	}

	public void setValor(float valor) {
		this.valor = valor;
	}

	public void setId(long id) {
		this.id = id;
	}

}
