package softplan.exercicio1;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;

/**
 * Implementação JPA do DAO utilizado no exercicio 1
 * 
 * @author Carlos Silva
 *
 */
public class ExercicioDAO {

	private EntityManager em = null;

	public ExercicioDAO() {
		em = Persistence.createEntityManagerFactory("exercicio1").createEntityManager();

		// inicializa as tabelas usadas
		em.getTransaction().begin();
		// tipos de via
		TipoVia tipoVia = new TipoVia();
		tipoVia.setId(1);
		tipoVia.setDescricao("Pavimentada");
		tipoVia.setCustoKmRodado((float) 0.54);
		em.merge(tipoVia);

		tipoVia = new TipoVia();
		tipoVia.setId(2);
		tipoVia.setDescricao("Não-Pavimentada");
		tipoVia.setCustoKmRodado((float) 0.56);
		em.merge(tipoVia);

		// tipos de veiculo
		TipoVeiculo tipoVeiculo = new TipoVeiculo();
		tipoVeiculo.setId(1);
		tipoVeiculo.setDescricao("Caminhao Bau");
		tipoVeiculo.setFator((float) 1.00);
		em.merge(tipoVeiculo);

		tipoVeiculo = new TipoVeiculo();
		tipoVeiculo.setId(2);
		tipoVeiculo.setDescricao("Caminhao Cacamba");
		tipoVeiculo.setFator((float) 1.05);
		em.merge(tipoVeiculo);

		tipoVeiculo = new TipoVeiculo();
		tipoVeiculo.setId(3);
		tipoVeiculo.setDescricao("Carreta");
		tipoVeiculo.setFator((float) 1.12);
		em.merge(tipoVeiculo);

		// parametros
		Parametro parametro = new Parametro();
		parametro.setId(Parametro.Chave.CARGA_MINIMA_TONELADAS.ordinal());
		parametro.setValor((float) 5.0);
		em.merge(parametro);

		parametro = new Parametro();
		parametro.setId(Parametro.Chave.CUSTO_TONELADA_EXTRA.ordinal());
		parametro.setValor((float) 0.02);
		em.merge(parametro);

		parametro = new Parametro();
		parametro.setId(Parametro.Chave.CUSTO_VIA_PAVIMENTADA.ordinal());
		parametro.setValor((float) 0.54);
		em.merge(parametro);

		parametro = new Parametro();
		parametro.setId(Parametro.Chave.CUSTO_VIA_NAO_PAVIMENTADA.ordinal());
		parametro.setValor((float) 0.62);
		em.merge(parametro);

		em.getTransaction().commit();

	}

	/**
	 * Recupera todos os registros da tabela inferida por entClass.
	 * 
	 * @param entClass
	 * @return {@link List} de {@link AbstractEntity}, se for uma classe gerenciada
	 * por esse DAO, ou null se a classe não corresponder a uma das tabelas.
	 */
	public List<AbstractEntity> findAll(Class<? extends AbstractEntity> entClass) {
		StringBuilder sb = new StringBuilder();
		if (em == null) {
			return null;
		}

		sb.append("SELECT e FROM ").append(entClass.getSimpleName()).append(" e ORDER BY id");

		try {
			@SuppressWarnings("unchecked")
			List<AbstractEntity> result = (List<AbstractEntity>) em.createQuery(sb.toString(), entClass)
					.getResultList();
			return result;
		} catch (IllegalArgumentException e) {
			return null;
		}
	}

	/**
	 * Recupera um objeto da tabela inferida a partir de entClass e pelo id.
	 * 
	 * @param entClass
	 * @param id
	 * @return {@link AbstractEntity}, se for uma classe gerenciada por esse DAO, ou
	 * null se a classe não corresponder a uma das tabelas.
	 */
	public AbstractEntity findById(Class<? extends AbstractEntity> entClass, long id) {
		StringBuilder sb = new StringBuilder();
		AbstractEntity result = null;
		if (em == null) {
			return null;
		}

		sb.append("SELECT e FROM ").append(entClass.getSimpleName()).append(" e WHERE e.id = :id");
		try {
			result = em.createQuery(sb.toString(), entClass).setParameter("id", id).getSingleResult();
			return result;
		} catch (IllegalArgumentException | NoResultException e) {
			return null;
		}
	}
}
