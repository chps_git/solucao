package softplan.exercicio1;

import static org.junit.Assert.*;

import org.junit.Test;

public class CalculoTest {

	@Test
	public void testCalcularCustoTransporte() {
		Calculo calculo = new Calculo ();
		assertTrue(calculo.calcularCustoTransporte(100.0f, 0.0f, 0.54f, 0.62f, 1.05f, 8.0f, 5.0f, 0.02f) == 62.70f);
		assertTrue(calculo.calcularCustoTransporte(0.0f, 60.0f, 0.54f, 0.62f, 1.00f, 4.0f, 5.0f, 0.02f) == 37.20f);
		assertTrue(calculo.calcularCustoTransporte(0.0f, 180.0f, 0.54f, 0.62f, 1.12f, 12.0f, 5.0f, 0.02f) == 150.19f);
		assertTrue(calculo.calcularCustoTransporte(80.0f, 20.0f, 0.54f, 0.62f, 1.00f, 6.0f, 5.0f, 0.02f) == 57.60f);
		assertTrue(calculo.calcularCustoTransporte(50.0f, 30.0f, 0.54f, 0.62f, 1.05f, 5.0f, 5.0f, 0.02f) == 47.88f);
	}

}
