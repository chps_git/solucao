package softplan.exercicio1;

/**
 * Classe criada apenas para testar o caso de uma entidade n~ao suportada pelo DAO.
 * @author Carlos Silva
 *
 */
public class Foobar implements AbstractEntity {

	@Override
	public long getId() {
		return 0;
	}

}
