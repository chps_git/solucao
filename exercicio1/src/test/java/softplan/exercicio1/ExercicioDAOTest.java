package softplan.exercicio1;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

public class ExercicioDAOTest {

	@Test
	public void testFindAll() {
		ExercicioDAO dao = new ExercicioDAO();
		
		// busca todos os itens de TipoVia 
		List<AbstractEntity> tbl1 = dao.findAll(TipoVia.class);
		assertTrue(tbl1 != null);
		assertTrue(tbl1.size() == 2);
		TipoVia ent1 = (TipoVia)tbl1.get(0);
		assertTrue(ent1.getId() == 1);
		assertTrue(ent1.getDescricao().compareTo("Pavimentada") == 0);
		assertTrue(ent1.getCustoKmRodado() == (float)0.54);
		
		// busca todos os itens para uma classe não suportada pelo DAO
		List<AbstractEntity> tbl2 = dao.findAll(Foobar.class);
		assertTrue(tbl2 == null);
	}

	@Test
	public void testFindById() {
		ExercicioDAO dao = new ExercicioDAO();
		
		// Busca pela entidade de id 1 para a classe TipoVia
		AbstractEntity item = dao.findById(TipoVia.class, 1);
		assertTrue (item != null);
		TipoVia ent1 = (TipoVia)item;
		assertTrue(ent1.getId() == 1);
		assertTrue(ent1.getDescricao().compareTo("Pavimentada") == 0);
		assertTrue(ent1.getCustoKmRodado() == (float)0.54);
		
		// Busca pela entidade de id 9999 para a classe TipoVia
		item = dao.findById(TipoVia.class, 9999);
		assertTrue (item == null);		
		
		// Busca pela entidade de id 1 para uma classe não suportada pelo DAO
		item = dao.findById(Foobar.class, 1);
		assertTrue(item == null);
	}

}
